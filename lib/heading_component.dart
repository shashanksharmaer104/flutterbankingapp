
import 'package:flutter/material.dart';

class HeadingComponent extends StatelessWidget {
  
  final String name;

  HeadingComponent({@required this.name});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomLeft,
      //margin: EdgeInsets.only(bottom: 12.0),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
                height: 30.0,
              ),
          Text(
            name,
            style: TextStyle(
              fontSize: 18.0,
              fontFamily: 'Helvetica Medium',
              //fontWeight: FontWeight.bold,
              color: Color(0xFF8E919E),
            ),
          ),
          Divider(
            color: Colors.white,
            thickness: 1.0,
          ),
        ],
      ),
    );
  }
}