import 'package:flutter/material.dart';

class RowComponent extends StatelessWidget {
  
  final String accName;
  final String accBalance;

  RowComponent({@required this.accName, @required this.accBalance});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        //print('Tapped log in !!!');
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(0, 5.0, 0, 5.0),
        width: double.infinity,
        height: 74.0,
        decoration: BoxDecoration(
          color: Color(0xFF163B8F),
          borderRadius: BorderRadius.circular(3.0),
        ),
        child: Padding(
          padding: EdgeInsets.all(15.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    accName,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontFamily: 'Helvetica Medium',
                      //fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    accBalance,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontFamily: 'Helvetica Medium',
                      //fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}