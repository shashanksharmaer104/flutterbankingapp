import 'package:flutter/material.dart';
import 'row_component.dart';
import 'heading_component.dart';
import 'subheading_component.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*drawer: Drawer(
        child: ListView(padding: EdgeInsets.zero, children: <Widget>[
          DrawerHeader(
              child: Text(
                'Side menu',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
              decoration: BoxDecoration(
                color: Colors.green,
              )),
          ListTile(
            leading: Icon(Icons.tv),
            title: Text('Welcome'),
            onTap: () => {},
          ),
        ]),
      ),*/
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0.0,
        centerTitle: true,
        backgroundColor: Color(0xFF0C2074),
        title: Text(
          'Accounts',
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'Helvetica Medium',
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Container(
        color: Color(0xFF0C2074),
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.fromLTRB(16.0, 0, 16.0, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Icon(
                    Icons.live_help,
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(
                height: 32.0,
              ),
              Container(
                alignment: Alignment.bottomLeft,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      'SUNDAY, MAY 31',
                      style: TextStyle(
                        fontFamily: 'Helvetica Medium',
                        fontWeight: FontWeight.normal,
                        color: Color(0xFF8E919E),
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Text(
                      'Welcome back,\nShashank.',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 35.0,
                        fontFamily: 'Helvetica Medium',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              /*SizedBox(
                height: 54.0,
              ),*/
              Container(
                child: Column(
                  children: <Widget>[
                    HeadingComponent(name: 'Assets'),
                    SubheadingComponent(name: 'CHECKING & SAVINGS'),
                    RowComponent(
                      accName: 'Checking ...1234',
                      accBalance: 'Rs. 123497.09',
                    ),
                    RowComponent(
                      accName: 'Checking ...0987',
                      accBalance: 'Rs. 1294.09',
                    ),
                    RowComponent(
                      accName: 'Savings ...9743',
                      accBalance: 'Rs. 0.0',
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    //HeadingComponent(name: 'INVESTMENTS & RETIREMENTS'),
                    SubheadingComponent(name: 'INVESTMENTS & RETIREMENTS'),
                    /*RowComponent(
                      accName: 'Advisory IRA ...1234',
                      accBalance: 'Rs. 123497.09',
                    ),*/
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.money_off, color: Color(0xFF0C2074)),
            title: Text('Transfers', style: TextStyle(color: Color(0xFF0C2074))),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business, color: Color(0xFF0C2074)),
            title: Text('Products', style: TextStyle(color: Color(0xFF0C2074))),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.mic, color: Color(0xFF0C2074)),
            title: Text('Voice', style: TextStyle(color: Color(0xFF0C2074))),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color(0xFF0C2074),
        //onTap: _onItemTapped,
      ),
    );
  }
}
