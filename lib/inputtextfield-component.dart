import 'package:flutter/material.dart';

class InputTextField extends StatelessWidget {
  /*const InputTextField({
    Key key,
    @required TextEditingController controller,
    @required String label,
  }) : _controller = controller, _userLabelText = label, super(key: key);*/


  final TextEditingController controller;
  final String label;
  final bool obscureText;

  InputTextField({this.controller, this.label, this.obscureText});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10.0),
      child: TextFormField(
        obscureText: obscureText,
        keyboardType: TextInputType.text,
        autofocus: false,
        textAlign: TextAlign.start,
        textInputAction: TextInputAction.done,
        //controller: nameTextEditingController,
        style: TextStyle(
          color: Color(0xFF0C2074),
          fontSize: 15,
        ),
        decoration: InputDecoration(
          /*suffixIcon: IconButton(
            onPressed: () => controller.clear(),
            icon: Icon(Icons.clear, color: Color(0xFF0C2074)),
          ),*/
          contentPadding: EdgeInsets.symmetric(vertical: 15),
          labelText: label,
          labelStyle: TextStyle(
            color: Colors.black54,
            fontSize: 15,
          ),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
            color: Color(0xFF0C2074),
            width: 1,
          )),
        ),
      ),
    );
  }
}