import 'package:flutter/material.dart';

class SubheadingComponent extends StatelessWidget {
  
  final String name;

  SubheadingComponent({@required this.name});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 12.0, 0, 12.0),
      width: double.infinity,
      child: Text(
      name,
      style: TextStyle(
        fontSize: 15.0,
        fontFamily: 'Helvetica Medium',
        //fontWeight: FontWeight.bold,
        color: Color(0xFF8E919E),
      ),
    ),
    );
  }
}