import 'package:flutter/material.dart';
import 'login_page.dart';

void main() => runApp(MobileApp());

class MobileApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.light().copyWith(
        primaryColor: Colors.white,
        primaryTextTheme: TextTheme(),
        scaffoldBackgroundColor: Colors.white,
        accentColor: Colors.blue,
      ),
      home: LoginPage(),
    );
  }
}
