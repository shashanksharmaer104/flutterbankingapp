import 'package:flutter/material.dart';
import 'package:flutter_banking_app/dashboard_page.dart';
import 'constants.dart';
import 'inputtextfield-component.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController nameTextEditingController = TextEditingController();
  String _userLabelText = "Username";
  String _passLabelText = "Password";
  bool isRememberSwitched = false;
  bool isFingerSwitched = false;
  Color activeButtonColor = Color(0xFF0941C5);

  var _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          'U.S. Bank Mobile App',
          style: TextStyle(
            color: Color(0xFF0C2074),
            fontFamily: 'Helvetica Bold',
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(16.0, 0, 16.0, 0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Icon(
                    Icons.live_help,
                    color: Color(0xFF0C2074),
                  ),
                ),
              ),
              SizedBox(
                height: 25.0,
              ),
              Container(
                //alignment: Alignment.centerLeft,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Hello!',
                      style: TextStyle(
                        color: Color(0xFF0C2074),
                        fontFamily: 'Helvetica Bold',
                        fontWeight: FontWeight.bold,
                        fontSize: 50.0,
                      ),
                    ),
                    Text(
                      'Glad you\'re here.',
                      style: TextStyle(
                        color: Color(0xFF0C2074),
                        fontFamily: 'Helvetica Bold',
                        fontSize: 25.0,
                        fontWeight: FontWeight.bold,
                        textBaseline: TextBaseline.alphabetic,
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(
                height: 40.0,
              ),
              InputTextField(
                  controller: _controller,
                  label: _userLabelText,
                  obscureText: false),
              InputTextField(
                  controller: _controller,
                  label: _passLabelText,
                  obscureText: true),
              Container(
                width: double.infinity,
                child: Row(
                  children: <Widget>[
                    Semantics(
                      value: "rememberMeSwitch",
                      //label: "rememberMeSwitch",
                      child: Switch(
                        value: isRememberSwitched,
                        onChanged: (value) {
                          setState(() {
                            isRememberSwitched = value;
                            print(isRememberSwitched);
                          });
                        },
                        //activeTrackColor: Color(0xFF8E919E),
                        activeColor: Color(0xFF163B8F),
                      ),
                    ),
                    Semantics(
                      value: "rememberMeLabel",
                      child: Text(
                        'Remember me',
                        style: TextStyle(
                          color: Colors.black54,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          'Login help',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color(0xFF0941C5),
                            //fontFamily: 'Helvetica Medium',
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                child: Row(
                  children: <Widget>[
                    Switch(
                      value: isFingerSwitched,
                      onChanged: (value) {
                        setState(() {
                          isFingerSwitched = value;
                          print(isFingerSwitched);
                        });
                      },
                      //activeTrackColor: Color(0xFF8E919E),
                      activeColor: Color(0xFF163B8F),
                    ),
                    Text(
                      //'Enable fingerprint ID (Remember me required)',
                      'Enable fingerprint ID',
                      style: TextStyle(
                        color: Colors.black54,
                      ),
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  print('Tapped log in !!!');
                  setState(() {
                    activeButtonColor = Color(0xFFD3D3D3);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Dashboard()));
                  });
                },
                child: Container(
                  child: Center(
                    child: Text(
                      kLoginButtonText,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                        fontFamily: 'Helvetica Medium',
                      ),
                    ),
                  ),
                  //color: Color(0xFF0941C5),
                  width: double.infinity,
                  height: 50.0,
                  decoration: BoxDecoration(
                    color: activeButtonColor,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Semantics(
                value: "loginButton",
                child: GestureDetector(
                  onTap: () {
                    print('Tapped tour the new app !!!');
                  },
                  child: Container(
                    child: Center(
                      child: Text(
                        kTourNewAppButtonText,
                        style: TextStyle(
                          color: Color(0xFF0941C5),
                          fontFamily: 'Helvetica Medium',
                        ),
                      ),
                    ),
                    //color: Color(0xFF0941C5),
                    width: double.infinity,
                    height: 50.0,
                    //margin: EdgeInsets.only(right: 10.0),
                  ),
                ),
              ),
              /*Container(
                height: 130.0,
                color: Colors.grey[300],
                margin: EdgeInsets.all(15.0),
                child: Container(
                  color: Colors.white,
                  height: 100.0,
                ),
                //child: ,
              ),*/
            ],
          ),
        ),
      ),
    );
  }
}
